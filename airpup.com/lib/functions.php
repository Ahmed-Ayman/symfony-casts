<?php
function addNewPet($pet)
{
	$name = $pet['name'];
	$breed = $pet['breed'];
	$weight = $pet['weight'];
	$bio = $pet['weight'];
	$image = 'pancake.png';
	$likes = 0;
	$pdo = get_connection();
	$query = 'INSERT INTO pet (name, breed, weight, bio,likes,image) VALUES (:name,:breed,:weight,:bio,:likes,:image)';
	$stmt = $pdo->prepare($query);
	$stmt->bindParam(":name", $name);
	$stmt->bindParam(":breed", $breed);
	$stmt->bindParam(":weight", $weight);
	$stmt->bindParam(":bio", $bio);
	$stmt->bindParam(":likes", $likes);
	$stmt->bindParam(":image", $image);

	$stmt->execute();

}

function get_pets($numberOfPets = 0)
{
	$pdo = get_connection();
	$query = 'SELECT * FROM pet';
	if ($numberOfPets) {
		$query = $query . 'LIMIT :numOfPets';
	}
	$stmt = $pdo->prepare($query);
	$stmt->bindParam('numOfPets', $numberOfPets);
	$stmt->execute();
	$pets = $stmt->fetchAll();
	return $pets;
}

function get_pet($id)
{
	$pdo = get_connection();
	$query = 'SELECT * FROM pet WHERE id = :petId';
	$stmt = $pdo->prepare($query);
	$stmt->bindParam('petId', $id);
	$stmt->execute();
	$pet = $stmt->fetch();
	return $pet;
}

function addLike($id)
{
	$pdo = get_connection();
	$query = 'UPDATE pet SET likes = likes + 1 WHERE id = :petId';
	$stmt = $pdo->prepare($query);
	$stmt->bindParam("petId", $id);
	$stmt->execute();
}

function subtractLike($id)
{
	$pdo = get_connection();
	$query = 'UPDATE pet SET likes = likes - 1 WHERE id = :petId';
	$stmt = $pdo->prepare($query);
	$stmt->bindParam("petId", $id);
	$stmt->execute();
}

function getLikes($id)
{

	$pdo = get_connection();
	$query = 'SELECT likes FROM pet WHERE id = :petId';
	$stmt = $pdo->prepare($query);
	$stmt->bindParam("petId", $id);
	$stmt->execute();
	$likes = $stmt->fetch();
	return $likes[0];

}

function get_connection()
{
	$config = require 'bootstrap/config.php';

	$pdo = new PDO(
		$config['dsn'],
		$config['db_user'],
		$config['db_passwd']
	);
	// setting the error mode to exception when working with the DB.
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $pdo;
}