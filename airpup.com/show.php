<?php
// TODO handle the likes.

/*
 * the likes logic:
 *  - once I click like me the anchor adds like=true.
 *  - once I click unlike me the anchor adds like=false.
 *  - like=true > add a like to the DB and get the likes.
 *  - like=false > subtract a like from the DB then get the likes.
 *  - (like > true) create a session variable  called liked to handle when liked already.
*/
require 'layout/header.php';
require 'lib/functions.php'; ?>
<?php
if (isset($_GET['q'])) {
	$id = (int)$_GET['q'];
} else {
	$id = null;
}
$pet = get_pet($id);
?>

    <article class="container">
        <div class="col-xs-4">
            <div class="">
                <img class="img-rounded img-responsive" src="images/<?= $pet['image'] ?>" alt="">
            </div>
        </div>
        <div class="col-xs-6">
            <table>
                <tr>
                    <td><strong>Name: </strong></td>
                    <td><?= $pet['name'] ?></td>
                </tr>
                <tr>
                    <td><strong>Age: </strong></td>
                    <td><?= $pet['age'] ?></td>
                </tr>
                <tr>
                    <td><strong>Breed: </strong></td>
                    <td><?= $pet['breed'] ?></td>
                </tr>
                <tr>
                    <td><strong>weight: </strong></td>
                    <td><?= $pet['weight'] ?></td>
                </tr>
                <tr>
                    <td><strong>Bio</strong></td>
                    <td><?= $pet['bio'] ?></td>
                </tr>
            </table>
        </div>

    </article>

<?php require 'layout/footer.php'; ?>