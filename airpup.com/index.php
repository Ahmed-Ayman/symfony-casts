<?php
// TODO: finialize the website, add more styling and remove the email form.
require 'lib/functions.php';
$cleverWelcomeMessage = 'All the love, none of the crap!';

$pets = get_pets();
$pupCount = count($pets);
?>
<?php require 'layout/header.php' ?>
<div class="jumbotron">
    <div class="container">
        <h1><?= strtolower($cleverWelcomeMessage); ?></h1>
        <p>with over <?= $pupCount; ?> pet friends!</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p>
    </div>
</div>
<div class="container">
    <div class="row">
		<?php foreach ($pets as $pet): ?>
            <div class="col-lg-4 pet-list-item">
                <h3>
                    <a href="show.php?q=<?= $pet['id'] ?>">
						<?= $pet['name'] ?>
                    </a>
                </h3>
                <img src="images/<?= $pet['image'] ?>" class="img-rounded" alt="beautiful puppy">
                <blockquote>
                    <span class="label label-info"><?= $pet['breed'] ?></span>
					<?php
					if (!array_key_exists('age', $pet) || $pet['age'] == '')
						echo "Unknown";
                    elseif ($pet['age'] == 'hidden')
						echo "(contact owner for age)";
					else
						echo $pet['age'];
					?>

					<?= $pet['weight'] ?> lbs
                </blockquote>
                <p><?= $pet['bio'] ?></p>
            </div>
		<?php endforeach ?>
    </div>
</div>
<?php require 'layout/footer.php' ?>

